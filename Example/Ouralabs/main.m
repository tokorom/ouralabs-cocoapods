//
//  main.m
//  Ouralabs
//
//  Created by Ryan Fung on 01/19/2015.
//  Copyright (c) 2014 Ryan Fung. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OUAppDelegate class]));
    }
}
