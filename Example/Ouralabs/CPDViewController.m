//
//  OUViewController.m
//  Ouralabs
//
//  Created by Ryan Fung on 01/19/2015.
//  Copyright (c) 2014 Ryan Fung. All rights reserved.
//

#import "OUViewController.h"

@interface OUViewController ()

@end

@implementation OUViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
