
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Ouralabs
#define COCOAPODS_POD_AVAILABLE_Ouralabs
#define COCOAPODS_VERSION_MAJOR_Ouralabs 0
#define COCOAPODS_VERSION_MINOR_Ouralabs 1
#define COCOAPODS_VERSION_PATCH_Ouralabs 0

