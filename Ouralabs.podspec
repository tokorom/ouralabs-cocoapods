#
# Be sure to run `pod lib lint Ouralabs.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Ouralabs"
  s.version          = "2.7.1"
  s.summary          = "Ouralabs is a device log management solution."
  s.homepage         = "https://www.ouralabs.com"
  s.license          = 'MIT'
  s.author           = { "Ouralabs" => "support@ouralabs.com" }
  s.source           = { :git => "https://bitbucket.org/ouralabs/ouralabs-cocoapods.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/ouralabs'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.frameworks = 'SystemConfiguration'
  s.library    = 'z'

  s.source_files = 'Pod/Classes'

  s.public_header_files = 'Pod/Classes/**/*.h'
end
